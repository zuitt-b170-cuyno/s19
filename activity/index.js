const num = 5, getCube = num ** 3
console.log(`The cube of ${num} is ${getCube}`)

const address = ["258 Washington Ave NW", "California", "90011"]
const [street, state, zipcode] = address
console.log(`I live at ${street}, ${state} ${zipcode}`)

const animal = {
    name: "Lolong",
    inhabitat: "saltwater",
    type: "crocodile",
    weight: 1075, // measured in kilogram
    measurement: 20.25 // measured in feet
}

const {name, inhabitat, type, weight, measurement} = animal
const feet = Math.floor(measurement), inch = feet => feet * 12
console.log(`${name} was a ${inhabitat} ${type}. He weighed at ${weight} kgs with a measurement of ${feet} ft ${inch(measurement - feet)} in.`)

const arr = [1, 2, 3, 4, 5], sum = arr.reduce((x, y) => x + y, 0)
arr.forEach(x => console.log(x))
console.log(sum)

class Dog {
    constructor(name, age, breed) {
        this.name = name
        this.age = age
        this.breed = breed
    }
}

console.log(new Dog("Frankie", 5, "Miniature Dachshund"))