// JS ES6 Updates

// Exponent Operator
// Pre-ES6
const firstNum = Math.pow(8, 2)
console.log(firstNum)

// Post-ES6
const secondNum = 8 ** 2
console.log(secondNum)

const username = "Johnny", message = `Welcome to the world of programming, ${username}`
console.log(message)

const anotherMessage = `${username} attended Math competition. He won it by solving the problem 8 ** 2 with the answer of ${8 ** 2}`
console.log(anotherMessage)

const interestRate = 0.10, principal = 1000
console.log(`Interest = ${principal * interestRate}`)

const fullName = ["Johnny", "Villamor", "Cuyno"]
fullName.forEach(x => console.log(x))

// Array Destructuring
const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)

// Object Destructuring
const womanName = {
    givenName: "Susan",
    maidenName: "Rendon",
    familyName: "Villamor"
}

console.log(womanName.givenName)
console.log(womanName.maidenName)
console.log(womanName.familyName)
console.log(`Full Name: ${womanName.givenName} ${womanName.maidenName} ${womanName.familyName}`)

const {givenName, maidenName, familyName} = womanName
console.log(givenName)
console.log(maidenName)
console.log(familyName)
console.log(`Full Name: ${givenName} ${maidenName} ${familyName}`)

const printFName = (fname, mname, lname) => { console.log(fname, mname, lname) }
printFName("Johnny", "Villamor", "Cuyno")

const students = ['John', 'Jane', 'Joe']
students.forEach(x => console.log(x))

const add = (x, y) => x + y
console.log(add(5, 6))

function addNumbers(x, y) {
    return x + y
}

console.log(addNumbers(5, 6))

const greet = (user="User") => {
    return `Good Morning, ${user}`
}

console.log(greet("Johnny"))

// function Car(name, brand, year) {
//     this.name = name
//     this.brand = brand
//     this.year = year
// }

class Car {
    constructor(brand, name, year) {
        this.brand = brand
        this.name = name
        this.year = year
    }
}

const car1 = new Car("Honda", "Johnny", 2022)
const car2 = new Car()

console.log(car1)

car2.brand = "Toyota"
car2.name = "Johnny"
car2.year = 2022

console.log(car2)

const number = 5
console.log(number <= 0)